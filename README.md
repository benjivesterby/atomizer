# Atomizer

[![Build Status](https://travis-ci.org/benji-vesterby/atomizer.svg?branch=master)](https://travis-ci.org/benji-vesterby/atomizer)
[![Go Report Card](https://goreportcard.com/badge/github.com/benji-vesterby/atomizer)](https://goreportcard.com/report/github.com/benji-vesterby/atomizer)
[![GoDoc](https://godoc.org/github.com/benji-vesterby/atomizer?status.svg)](https://godoc.org/github.com/benji-vesterby/atomizer)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)


Atomizer is a library that facilitates the execution of distributed atomic actions across a cluster of nodes.
