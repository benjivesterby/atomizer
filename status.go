package atomizer

const (
	// PENDING status for bonded atom electron running instance
	PENDING int = iota

	// PROCESSING status for bonded atom electron running instance
	PROCESSING

	// COMPLETED status for bonded atom electron running instance
	COMPLETED
)
